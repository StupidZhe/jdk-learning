package com.stupidzhe.jdklearning.reflect;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mr.W on 2017/11/1.
 */
class TestClass {

    public Object proxied;

    private String name;

    private static String staticName;

    private static final String staticFinalName = "Jack";

    protected String age;

    boolean sex;

    public TestClass(Object proxied) {
        this.proxied = proxied;
    }

    public TestClass() {

    }


    public String getName() {
        return name;
    }

    public static void staticPrint() {
        System.out.println("static hello");
    }

    public void print() {
        System.out.println("hello");
    }
}

public class MainClass {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        Class<TestClass> testClassClass = TestClass.class;
        TestClass testClass = testClassClass.newInstance();
        Field[] fields = testClassClass.getDeclaredFields();
        for (Field field : fields) {
            System.out.print("域名: " + field.getName());
            System.out.print(" 作用域: " + field.getModifiers());
            field.setAccessible(true);
            System.out.println(" 值" + field.get(testClass));
//            field.set(testMain, null);
        }
        try {
            Field name = testClassClass.getDeclaredField("name");
            name.setAccessible(true);
            name.set(testClass, "wangwang");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        Constructor<?>[] constructors = testClassClass.getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            try {
                List<Object> parameterList = new ArrayList<>(2);
                for (Parameter parameter : constructor.getParameters()) {
                    parameterList.add(parameter.getType().newInstance());
                }
                if (0 != parameterList.size()) {
                    TestClass testClass1 = (TestClass) constructor.newInstance(parameterList);
                    System.out.println(testClass1);
                } else {
                    // 无参构造器
                    TestClass testClass1 = (TestClass) constructor.newInstance();
                    System.out.println(testClass1);
                }
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        Method[] methods = testClassClass.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println("方法名: " + method.getName());
            System.out.println("作用域: " + method.getModifiers());
            List<Object> parameterList = new ArrayList<>(2);
            for (Parameter parameter : method.getParameters()) {
                parameterList.add(parameter.getType().newInstance());
            }
            try {
                if (0 != parameterList.size()) {
                    System.out.println(method.invoke(testClass, parameterList));
                } else {
                    System.out.println(method.invoke(testClass));
                }
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }
}
