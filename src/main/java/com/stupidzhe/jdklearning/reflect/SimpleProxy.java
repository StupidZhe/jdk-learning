package com.stupidzhe.jdklearning.reflect;

/**
 * Created by Mr.W on 2017/11/3.
 */
public class SimpleProxy {
}

// 代理类和委托类公用的接口
interface FInterface1 {
    void print();
}

// 委托类
class ProxiedClass implements FInterface1 {


    @Override
    public void print() {
        System.out.println("ProxiedClass print");
    }
}

// 代理类
class ProxyClass implements FInterface1 {

    private FInterface1 fInterface;

    private ProxyClass (FInterface1 fInterface) {
        this.fInterface = fInterface;
    }

    private ProxyClass() {
        fInterface = new ProxiedClass();
    }

    @Override
    public void print() {
        System.out.println("Before print");
        fInterface.print();
        System.out.println("After print");
    }

    public static void main(String... args) {
        ProxyClass proxyClass = new ProxyClass();
        proxyClass.print();
        /**-----------output-----------
                Before print
        ProxiedClass print
        After print
        ----------------------------**/
    }
}
