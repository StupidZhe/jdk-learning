package com.stupidzhe.jdklearning.util;

import java.util.*;

/**
 * Created by Mr.W on 2017/10/28.
 */
public class Collection  {
    public static void main(String[] args) {
        /*
        ArrayList<Integer> collection = new ArrayList<>(Arrays.asList(1,2,4));
        Integer[] a = {1,2,3};
        collection.addAll(Arrays.asList(a));
        System.out.println(collection.get(0));
        Collections.addAll(collection, 1, 2,3,4);
        System.out.println(collection.size());
        Iterator<Integer> iterator = collection.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
            iterator.remove();
        }
*/
        LinkedList<Integer> linkedList = new LinkedList<>(Arrays.asList());

        // addLast()、add()将某元素插入到列表的尾部，addFirst()将元素插入到列表的头部
        linkedList.addLast(3);
        linkedList.addFirst(1);
        linkedList.add(2);

        // getFirst()和element()、peek()都是返回列表的第一个元素
        // 区别在于当集合为空时，peek()返回null，而其他抛出NoSuchElementException
        System.out.println(linkedList.getFirst());
        System.out.println(linkedList.element());
        System.out.println(linkedList.peek());

        // removeFirst()和remove()、poll()都是返回并移除列表的第一个元素
        // 区别在于当集合为空时，peek()返回null，而其他抛出NoSuchElementException
        System.out.println(linkedList.removeFirst());
        System.out.println(linkedList.remove());
        System.out.println(linkedList.poll());

        // 返回并移除列表的最后一个元素
//        System.out.println(linkedList.removeLast());

        HashSet<Integer> linkedHashSet = new HashSet<>();
        linkedHashSet.add(8);
        linkedHashSet.add(7);
        linkedHashSet.add(6);
        System.out.println(linkedHashSet);

        PriorityQueue<Integer> queue = new PriorityQueue<>();
        queue.offer(2);
        queue.add(1);
        System.out.println(queue.peek());
    }
}

