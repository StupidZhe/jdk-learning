package com.stupidzhe.jdklearning.RTTI;

import java.util.Random;

/**
 * Created by Mr.W on 2017/11/1.
 */
public class ClassA extends ClassB {
    static {
        System.out.println("ClassA is loaded");
    }

    public static final int name = new Random(System.currentTimeMillis()).nextInt(100);
}
