package com.stupidzhe.jdklearning.RTTI.p;

import com.stupidzhe.jdklearning.RTTI.ClassA;
import com.stupidzhe.jdklearning.RTTI.ClassB;

/**
 * Created by Mr.W on 2017/11/1.
 */
public class TestMain {

    @SuppressWarnings("all")
    public static void main(String[] args) throws ClassNotFoundException {
       Class<ClassA> classAClass = ClassA.class;
        System.out.println(ClassA.name);
        System.out.println(classAClass.isInstance(ClassB.class));
//        Class intClass = int.class;
//        Class<Integer> a = Integer.class;
//        System.out.println(a.newInstance());
//        Class<? super ClassA> c = ClassA.class.getSuperclass();
//        System.out.println(c.getSimpleName());
    }
}
