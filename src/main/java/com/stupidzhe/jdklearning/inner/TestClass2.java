package com.stupidzhe.jdklearning.inner;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Mr.W on 2017/10/27.
 */
public class TestClass2 {
    public static void main(String[] args) {
         TestClass testClass = new TestClass();
         FInterface fInterface = testClass.getF();
         fInterface.print("1");
        new TestClass2().test(1);

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1,2,4,5,6));
        for (Integer e : list) {
            System.out.println(e);
        }

    }



    private int a = 1;

    private TestClass test(final int a) {
        return new TestClass() {
            private int i = a;
        };
    }
}
