package com.stupidzhe.jdklearning.inner;

/**
 * Created by Mr.W on 2017/10/25.
 *
 * @author Mr.W
 * @version 1.0
 * @see String.CaseInsensitiveComparator#chars()
 * @since 1.0
 */
public class TestClass {

    private int i = 1;

    static {
        System.out.println("TestClass is init");
    }

    {
        print();
    }

    private void print() {
        System.out.println("this is a public class");
        PrivateClass privateClass = new PrivateClass();
        privateClass.print();
    }

    protected class ProtectedClass {
        public void print() {
            System.out.println("this is a protected class");
        }
    }

    private class PrivateClass extends ProtectedClass implements FInterface {
        public void print() {
            super.print();
            System.out.println("this is a private class");
        }

        @Override
        public void print(String content) {
            System.out.println("hehe");
        }
    }

    public FInterface getF() {
        return new PrivateClass();
    }

    public PrivateClass getPrivateClass() {
        return new PrivateClass();
    }

    public static class StaticClass {

    }

    public static void main(String[] args) {
        TestClass testClass = new TestClass();
        PrivateClass p = testClass.new PrivateClass();
        p.print();
    }

}
