package com.stupidzhe.jdklearning.inner;

/**
 * Created by Mr.W on 2017/10/27.
 */
public interface FInterface {
    int r = 0;

    void print(String content);

    class A implements FInterface {

        @Override
        public void print(String content) {

        }
        class B extends A {
            @Override
            public void print(String content) {
                super.print(content);
            }
        }
    }
}
