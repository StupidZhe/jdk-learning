package com.stupidzhe.jdklearning.inner;

/**
 * Created by Mr.W on 2017/10/27.
 */
public abstract class AbClass extends TestClass implements FInterface {

    public AbClass() {
        hello();
    }
    static {
        System.out.println("AbClass被初始化");
    }

    // 这是一个抽象方法
    public abstract void print(String content);

    public void hello() {
        System.out.println("这是一个具有方法体的普通方法");
    }

}