package com.stupidzhe.jdklearning.inner;

/**
 * Created by Mr.W on 2017/10/27.
 */
public abstract class CoClass extends AbClass {
    class InnerClass {
        public void print() {
            System.out.println();
        }
    }
}
