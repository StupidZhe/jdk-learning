package com.stupidzhe.jdklearning.inner;

/**
 * Created by Mr.W on 2017/10/27.
 */
public class CoClass2 extends CoClass {

    public CoClass2() {
        super.hello();
    }
    @Override
    public void print(String content) {
    }

    public static void main(String[] args) {
        CoClass2 coClass2 = new CoClass2();
    }
}
