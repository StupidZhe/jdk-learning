package com.stupidzhe.jdklearning.annotation.anno;

import com.stupidzhe.jdklearning.enu.Animal;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface TestAnno {
    int age() default -1;
    float height();
    boolean sex();
    String[] array();
    Animal animal();
    TypeAnno anno();
}
@TestAnno(age = 18, height = 173, sex = true, array = {"1", "2"}, animal = Animal.MONKEY, anno = @TypeAnno)
class Test {
}
