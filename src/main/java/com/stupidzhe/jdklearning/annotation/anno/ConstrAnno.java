package com.stupidzhe.jdklearning.annotation.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.CONSTRUCTOR)
public @interface ConstrAnno {
    int number() default -1;
}
