package com.stupidzhe.jdklearning.annotation.orm_test.processor;

import com.stupidzhe.jdklearning.annotation.orm_test.anno.Constraints;
import com.stupidzhe.jdklearning.annotation.orm_test.anno.DBTable;
import com.stupidzhe.jdklearning.annotation.orm_test.anno.SQLInteger;
import com.stupidzhe.jdklearning.annotation.orm_test.anno.SQLString;
import com.stupidzhe.jdklearning.annotation.orm_test.exception.TableCreateException;
import com.stupidzhe.jdklearning.annotation.orm_test.exception.TableException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/16
 * @Description: 通过读取类信息以及数据库注解生成sql命令
 */

public class TableCreator {

    public static void main(String[] args) throws TableException {
        try {
            Class memberClass = Class.forName("com.stupidzhe.jdklearning.annotation.orm_test.Member");
            DBTable dbTable = (DBTable) memberClass.getAnnotation(DBTable.class);
            if (dbTable == null) {
                throw new TableCreateException("No DBTable annotation in class " + memberClass.getName());
            }

            String tableName = dbTable.value();
            if (tableName.length() == 0) {
                tableName = memberClass.getName().toUpperCase();
            }

            List<String> columnList = new ArrayList<>(10);
            for (Field field : memberClass.getDeclaredFields()) {
                Annotation[] fieldAnnotations = field.getDeclaredAnnotations();
                String columnName;
                for (Annotation fieldAnnotation : fieldAnnotations) {
                    if (SQLString.class.isInstance(fieldAnnotation)) {
                        SQLString sqlString = (SQLString) fieldAnnotation;
                        if (sqlString.name().trim().length() < 1) {
                            columnName = field.getName();
                        } else {
                            columnName = sqlString.name().trim();
                        }
                        columnList.add(columnName + " VARCHAR(" + sqlString.value() + ")" +
                                getConstraints(sqlString.constraints()) + ",");
                    }
                    if (SQLInteger.class.isInstance(fieldAnnotation)) {
                        SQLInteger sqlInteger = (SQLInteger) fieldAnnotation;
                        if (sqlInteger.name().trim().length() < 1) {
                            columnName = field.getName();
                        } else {
                            columnName = sqlInteger.name().trim();
                        }
                        columnList.add(columnName + " INT" + getConstraints(sqlInteger.constraints()) + ",");
                    }
                }
            }
            StringBuilder builder = new StringBuilder(120);
            builder.append("CREATE TABLE ");
            builder.append(tableName);
            builder.append(" ( ");
            for (String colContent : columnList) {
                builder.append(colContent);
            }
            String res = builder.substring(0, builder.length() - 1) + " )";
            System.out.println(res);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    private static String getConstraints(Constraints constraints) {
        StringBuilder res = new StringBuilder(30);
        if (!constraints.allowNull()) {
            res.append(" NOT NULL ");
        }
        if (constraints.primaryKey()) {
            res.append(" PRIMARY KEY");
        }
        if (constraints.unique()) {
            res.append(" UNIQUE ");
        }
        return res.toString();
    }

}
