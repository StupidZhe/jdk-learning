package com.stupidzhe.jdklearning.annotation.orm_test;

import com.stupidzhe.jdklearning.annotation.orm_test.anno.Constraints;
import com.stupidzhe.jdklearning.annotation.orm_test.anno.DBTable;
import com.stupidzhe.jdklearning.annotation.orm_test.anno.SQLInteger;
import com.stupidzhe.jdklearning.annotation.orm_test.anno.SQLString;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/15
 * @Description:
 */
@DBTable("member")
public class Member {

    @SQLString(13)
    private String firstName;

    @SQLString(12)
    private String lastName;

    @SQLInteger(constraints = @Constraints(primaryKey = true))
    private int age;

    private static int memberCount;

    public static int getMemberCount() {
        return memberCount;
    }

    public static void setMemberCount(int memberCount) {
        Member.memberCount = memberCount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
