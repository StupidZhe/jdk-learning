package com.stupidzhe.jdklearning.annotation.orm_test.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/15
 * @Description: 告诉注解器要生成一个数据库表
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DBTable {

    String value() default "";

}
