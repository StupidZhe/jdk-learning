package com.stupidzhe.jdklearning.annotation.orm_test.exception;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/16
 * @Description:
 */
public class TableCreateException extends TableException {
    public TableCreateException(String content) {
        super(content);
    }
}
