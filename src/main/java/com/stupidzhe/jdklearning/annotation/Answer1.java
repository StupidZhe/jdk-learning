package com.stupidzhe.jdklearning.annotation;

import com.stupidzhe.jdklearning.annotation.anno.ConstrAnno;
import com.stupidzhe.jdklearning.annotation.anno.ParamAnno;
import com.stupidzhe.jdklearning.annotation.anno.TypeAnno;

@TypeAnno(isRight = true)
public class Answer1 {

    @TypeAnno(isRight = true)
    private int a;

    @ConstrAnno
    public Answer1(@ParamAnno(name = "a", request = false) int a) {
        this.a = a;
    }

    public void print() {
        System.out.println(a);
    }

}
