package com.stupidzhe.jdklearning.annotation;

import com.stupidzhe.jdklearning.annotation.anno.TypeAnno;

public class AnnoTestClass {

    public static void main(String[] args) {
        Class<Answer1> answer1Class = Answer1.class;

        TypeAnno typeAnno = answer1Class.getAnnotation(TypeAnno.class);
        System.out.println(typeAnno.isRight());

    }


    @Override
    public String toString() {
        return super.toString();
    }
}
