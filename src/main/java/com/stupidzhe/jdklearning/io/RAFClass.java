package com.stupidzhe.jdklearning.io;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by Mr.W on 2017/11/9.
 */
public class RAFClass {
    public static void main(String[] args) throws IOException {
            RandomAccessFile randomAccessFile = new RandomAccessFile("./src/com/stupidzhe/jdklearning/io/file-test/file3.c"
            , "rw");

        randomAccessFile.writeChars("hello");
        randomAccessFile.seek(5);
        System.out.println((char) randomAccessFile.read());
    }
}
