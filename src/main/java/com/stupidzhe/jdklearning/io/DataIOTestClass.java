package com.stupidzhe.jdklearning.io;

import java.io.*;

/**
 * Created by Mr.W on 2017/11/10.
 */
public class DataIOTestClass {
    public static void main(String[] args) {
        try {
            // dataOutputStream.writeUTF
            // 使用的是UTF-8编码格式进行写入
            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("./src/com/stupidzhe/jdklearning/io/file-test/file1.c"));
            dataOutputStream.writeUTF("你好\nworld");
            dataOutputStream.close();
            // readUTF使用的是UTF-8编码格式进行读取
            // 因为其他read默认用的是utf-16写入，所以直接使用readUTF会报错
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream("./src/com/stupidzhe/jdklearning/io/file-test/file1.c"));
            System.out.println(dataInputStream.readUTF());
            dataInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
