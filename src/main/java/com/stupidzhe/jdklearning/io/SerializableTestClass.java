package com.stupidzhe.jdklearning.io;

import java.io.*;

public class SerializableTestClass implements Serializable {

    //    public SerializableTestClass () {
//        System.out.println(this.hashCode());
//    }
    public SerializableTestClass(String content) {
        this.content = content;
        System.out.println(this.hashCode());
    }

    private String content;

    public static void main(String[] args) {
        try {
            File file = new File("./src/com/stupidzhe/jdklearning/io/file-test/file11.c");
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(new SerializableTestClass("content = \"test content\""));
            objectOutputStream.writeObject(new SerializableTestClass("content = \"test content1\""));
            objectOutputStream.flush();
            objectOutputStream.close();
            fileOutputStream.close();

            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            SerializableTestClass serializableTestClass1 = (SerializableTestClass) objectInputStream.readObject();
            SerializableTestClass serializableTestClass2 = (SerializableTestClass) objectInputStream.readObject();
            System.out.println(serializableTestClass1.content);
            System.out.println(serializableTestClass2.content);
            System.out.println(serializableTestClass1.hashCode());
            System.out.println(serializableTestClass2.hashCode());
            objectInputStream.close();
            fileInputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    //    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(content);
    }

    //    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        this.content = (String) in.readObject();
    }

//    private void writeObject(ObjectOutputStream stream) throws IOException {
//        System.out.println(1);
//    }
//
//    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
//        System.out.println(2);
//    }
}

