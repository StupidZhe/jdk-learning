 h e l l o






<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<meta name="author" content="Chenwei" />
<meta name="robots" content="all" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="X-UA-Compatible"content="IE=EmulateIE7" />
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" type="text/css" href="style.css">
<script type="text/javascript" src="../js/My97DatePicker/WdatePicker.js"></script>
<title>欢迎访问温州医科大学就业创业信息网</title>
<script>
</script>
<style>
.search-form {	 
				padding: 10px;
}
.ipt {
		border: 1px solid #B9D1F0;
		margin: 0px 5px;
		font-size: 12px;
}
.search-form input {
		padding: 1px;
}
.shortdwmc
{
	 width:70%;
	 white-space:nowrap;
	 word-break:keep-all;
	 overflow:hidden;
	 text-overflow:ellipsis;
}
</style>
</head>
 
	

		
<body>
<div class="home-wallpaper wallpaper" style="width:98%;height:100%">
	
			
			 
			 <form action="<portlet:renderURL/>" method="post" id="f&lt;portlet:namespace/&gt;">
			 </form>
			 
				<div class="article-lists" align="left">
					 <ul>
						
						 
					 
										
						
								 	  <li>
								 	  	<span class="article" style="float:left;color:#676767;">
								 	    
								 	  </span>
											<span class="shortdwmc" style="float:left;width:290px;">&nbsp;
												<a href="home-article.jsp?ID=63464fdb-46b0-11e7-8340-73a98614f47a&type=xyzp&XH=0" target="_blank">
													桂林电子科技大学长期招聘优秀博士研究生，待遇上不封顶</a>
											 </span>
											 <span style="float:right">2017-06-01</span>   
											 <span style="float:right;width:250px;" class="shortdwmc">桂林电子科技大学&nbsp;&nbsp;</span>                        
										</li>
								 
								 	  <li>
								 	  	<span class="article" style="float:left;color:#676767;">
								 	    
								 	  </span>
											<span class="shortdwmc" style="float:left;width:290px;">&nbsp;
												<a href="home-article.jsp?ID=a1b9d4b2-468e-11e7-8340-73a98614f47a&type=xyzp&XH=1" target="_blank">
													九江学院2017年诚聘高层次人才简章</a>
											 </span>
											 <span style="float:right">2017-06-01</span>   
											 <span style="float:right;width:250px;" class="shortdwmc">九江学院&nbsp;&nbsp;</span>                        
										</li>
								 
								 	  <li>
								 	  	<span class="article" style="float:left;color:#676767;">
								 	    
								 	  </span>
											<span class="shortdwmc" style="float:left;width:290px;">&nbsp;
												<a href="home-article.jsp?ID=5a46fd38-46a1-11e7-8340-73a98614f47a&type=xyzp&XH=2" target="_blank">
													广州大学诚邀海外优秀人才申报“青年千人计划”</a>
											 </span>
											 <span style="float:right">2017-06-01</span>   
											 <span style="float:right;width:250px;" class="shortdwmc">广州大学&nbsp;&nbsp;</span>                        
										</li>
								 
								 	  <li>
								 	  	<span class="article" style="float:left;color:#676767;">
								 	    
								 	  </span>
											<span class="shortdwmc" style="float:left;width:290px;">&nbsp;
												<a href="home-article.jsp?ID=6d08a519-468f-11e7-8340-73a98614f47a&type=xyzp&XH=3" target="_blank">
													浙江大学国际校区2017年行政人员招聘启事</a>
											 </span>
											 <span style="float:right">2017-06-01</span>   
											 <span style="float:right;width:250px;" class="shortdwmc">浙江大学国际校区&nbsp;&nbsp;</span>                        
										</li>
								 
								 	  <li>
								 	  	<span class="article" style="float:left;color:#676767;">
								 	    
								 	  </span>
											<span class="shortdwmc" style="float:left;width:290px;">&nbsp;
												<a href="home-article.jsp?ID=dcf603f5-468e-11e7-8340-73a98614f47a&type=xyzp&XH=4" target="_blank">
													上海市政工程设计研究总院集团广东有限公司2017校园招聘简章</a>
											 </span>
											 <span style="float:right">2017-06-01</span>   
											 <span style="float:right;width:250px;" class="shortdwmc">上海市政工程设计研究总院集团广东有限公司&nbsp;&nbsp;</span>                        
										</li>
								 
								 	  <li>
								 	  	<span class="article" style="float:left;color:#676767;">
								 	    
								 	  </span>
											<span class="shortdwmc" style="float:left;width:290px;">&nbsp;
												<a href="home-article.jsp?ID=ea185a15-45de-11e7-8340-73a98614f47a&type=xyzp&XH=5" target="_blank">
													温州医科大学附属台州妇女儿童医院（台州市妇幼保健院）公开招聘编外工作人员</a>
											 </span>
											 <span style="float:right">2017-05-31</span>   
											 <span style="float:right;width:250px;" class="shortdwmc">温州医科大学附属台州妇女儿童医院&nbsp;&nbsp;</span>                        
										</li>
								 
								 	  <li>
								 	  	<span class="article" style="float:left;color:#676767;">
								 	    
								 	  </span>
											<span class="shortdwmc" style="float:left;width:290px;">&nbsp;
												<a href="home-article.jsp?ID=29eb67d8-45e5-11e7-8340-73a98614f47a&type=xyzp&XH=6" target="_blank">
													合肥工业大学宣城校区2017年招聘行政人员及辅导员人员启事</a>
											 </span>
											 <span style="float:right">2017-05-31</span>   
											 <span style="float:right;width:250px;" class="shortdwmc">合肥工业大学&nbsp;&nbsp;</span>                        
										</li>
								 
								 	  <li>
								 	  	<span class="article" style="float:left;color:#676767;">
								 	    
								 	  </span>
											<span class="shortdwmc" style="float:left;width:290px;">&nbsp;
												<a href="home-article.jsp?ID=ea2e78b0-459f-11e7-8340-73a98614f47a&type=xyzp&XH=7" target="_blank">
													明峰医疗系统股份有限公司招聘公告</a>
											 </span>
											 <span style="float:right">2017-05-31</span>   
											 <span style="float:right;width:250px;" class="shortdwmc">明峰医疗系统股份有限公司&nbsp;&nbsp;</span>                        
										</li>
								 
								 	  <li>
								 	  	<span class="article" style="float:left;color:#676767;">
								 	    
								 	  </span>
											<span class="shortdwmc" style="float:left;width:290px;">&nbsp;
												<a href="home-article.jsp?ID=4422b85e-45e6-11e7-8340-73a98614f47a&type=xyzp&XH=8" target="_blank">
													铜陵市委党校2017年6月招聘</a>
											 </span>
											 <span style="float:right">2017-05-31</span>   
											 <span style="float:right;width:250px;" class="shortdwmc">铜陵市委党校&nbsp;&nbsp;</span>                        
										</li>
								 
								 	  <li>
								 	  	<span class="article" style="float:left;color:#676767;">
								 	    
								 	  </span>
											<span class="shortdwmc" style="float:left;width:290px;">&nbsp;
												<a href="home-article.jsp?ID=016d3c6b-45e6-11e7-8340-73a98614f47a&type=xyzp&XH=9" target="_blank">
													烟台黄金职业学院2017年度专任教师岗招聘计划</a>
											 </span>
											 <span style="float:right">2017-05-31</span>   
											 <span style="float:right;width:250px;" class="shortdwmc">烟台黄金职业学院&nbsp;&nbsp;</span>                        
										</li>
								 
							
					 
						
					
					</ul>
					
					
					
					
					
					
						
					 
						
							<!---->
							<div class="artilce-nav">
		          <a style='width:40px;color:#0082e7'>3319/10</a>
			        <a style='width:50px;color:#0082e7;' href=home.jsp?type=xyzp&pageNow=1>首页</a>
		          
		          
		          
		          <a style='width:20px;color:red;font-weight:bold;'>1</a>	
		          
		            <a style='width:50px;color:#0082e7;' href=home.jsp?type=xyzp&pageNow=2>下一页</a>
		          
		          <a style='width:50px;color:#0082e7;' href=home.jsp?type=xyzp&pageNow=332>尾页</a>
		          转到 <input type="text" id="curPage"  style="width:20px;"  name="curPage" value="1" /> 页
		          <input type="button"  style="width:25px;" value="Go" onclick="turnToPage('xyzp')"/>
		          <input type="hidden" id="pageCount"  name="pageCount" value="332" />
	         </div>
							
						
	         
				 
					
				 </div>
		 
	</div>
 
</div>
</body> 
</html>
 <script type="text/javascript">
 	function turnToPage(pType){
 	var curPage = document.getElementById("curPage").value;
 	var pageCount = document.getElementById("pageCount").value;
 	if(curPage=="" || curPage==null){
 	  alert("请输入需要转到的页数！");
 	  document.getElementById("curPage").focus();
 	  return false;	
 	}
	if(curPage*1>pageCount*1){
		alert("最大页数为"+pageCount+"，请重新输入！");
		document.getElementById("curPage").value="";
		document.getElementById("curPage").focus();
		return false;	
	}
	var goUrl = window.location.href;
	var ind = goUrl.indexOf("&pageNow=",0);
	if(ind>0){
				var url_slice = goUrl.substr(0, ind);
				url_slice =url_slice +"&pageNow="+curPage;
				goUrl = url_slice;
	 }else{
			goUrl =goUrl+"&pageNow="+curPage;
	}
	if(document.getElementById("DZPHBH")){
		var DZPHBH = document.getElementById("DZPHBH").value;
		if(DZPHBH!=null && DZPHBH!=""){
			goUrl = goUrl+"&DZPHBH="+DZPHBH;
		 }	
	}
	window.open(goUrl,"_self");
 }
 function resetQuery(){
 	  if(document.getElementById("zphzt")){
 	  	document.getElementById("zphzt").value="";
 	  }
 	  if(document.getElementById("sfgq")){
 	  	document.getElementById("sfgq").value="";
 	  }
 	  if(document.getElementById("jbksrq")){
 	  	document.getElementById("jbksrq").value="";
 	  }
 	   if(document.getElementById("jbjsrq")){
 	  	document.getElementById("jbjsrq").value="";
 	  }
 }
 function turnToNextPage(type,pageNow){
 	var form = document.getElementById("zphsearchingForm");
 	document.getElementById("pageNow").value=pageNow;
 	form.submit();
 	}
 function sercheUrl(){
 	 var sfss =document.getElementById("sfss").value;
 	 var zphzt =document.getElementById("zphzt").value;
 	 var jbksrq =document.getElementById("jbksrq").value;
 	 var jbjsrq =document.getElementById("jbjsrq").value;
 	 var sfgq =document.getElementById("sfgq").value;
 	 var searchUrl ="";
 	 var url = window.location.href;
	 var ind ;
	 ind = url.indexOf("/cmstar",0);
	 if(ind>0){
				var url_slice_potal=  url.substr(0, ind);
				var hre_url_portal  = url_slice_potal+"/cmstar/";
				searchUrl = hre_url_portal;
		}else{
			 ind = url.indexOf("/",0);
			 var url_slice=  url.substr(0, ind);
			 var hre_url  = url_slice+"/";
			 searchUrl = hre_url;
		}
	 if(searchUrl!=""&&searchUrl!=null){
	 	searchUrl += "login/xmu/home.jsp?type=zph&pageNow=1&sfss="+sfss+"&zphzt="+zphzt+"&jbksrq="+jbksrq+"&jbjsrq="+jbjsrq+"&sfgq="+sfgq+"&pageSearch=1";
	 }
 	 window.open(searchUrl,'_self');
 	}
 </script>
 