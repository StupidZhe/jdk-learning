package com.stupidzhe.jdklearning.io;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class XMLTestClass {
    public static void main(String[] args) {

        Map<Character, Character> map = new HashMap<>(2);
        map.put('[', ']');
        map.put('(', ')');

        Map<Character, Character> map1 = new HashMap<>(2);
        map1.put('[', ')');
        map1.put('(', ']');
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        if (count < 0 || count > 100) {
            return;
        }
        String[] contents = new String[count];
        for (int n = 0; n < count; n++) {
            contents[n] = scanner.next();
        }
        for (String content : contents) {
            if (!content.matches("[\\[|\\)|\\(|\\]]+")) {
                System.out.println("no");
                continue;
            }
            char[] contentArray = content.toCharArray();
            for (int m = contentArray.length - 1; m > -1; m--) {
                char tmp = contentArray[m];
                if (tmp == '(' || tmp == '[') {
                    contentArray[m] = ' ';
                    for (int l = m + 1; m < contentArray.length; l++) {
                        if (contentArray[l] == map1.get(tmp)) {
                            break;
                        }
                        if (contentArray[l] == map.get(tmp)) {
                            contentArray[l] = ' ';
                            break;
                        }
                    }
                }
            }
            if (new String(contentArray).trim().length() == 0) {
                System.out.println("yes");
            } else {
                System.out.println("no");
            }
        }

    }

}
