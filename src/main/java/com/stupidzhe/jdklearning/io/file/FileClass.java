package com.stupidzhe.jdklearning.io.file;

import java.io.File;

/**
 * Created by Mr.W on 2017/11/8.
 */
public class FileClass {

    public static void main(String[] args) {
        File file = new File("./src/com/stupidzhe/jdklearning/io/file-test/t");
        if (!file.exists()) {
            System.out.println("this is not a file");
            return;
        }
        System.out.println("is Directory: " + file.isDirectory());
        System.out.println("is File: " + file.isFile());
        System.out.println("its parent: " + file.getParent());
        System.out.println("can write: " + file.canWrite());
        System.out.println("can read: " + file.canRead());
        System.out.println("absolute path: " + file.getAbsolutePath());
        System.out.println("last modify: " + file.lastModified());
        if (!file.delete()) {
            System.out.println("delete file fail");
            return;
        }
        file = new File("./src/com/stupidzhe/jdklearning/io/file-test/t");
        if (file.exists()) {
            return;
        }
        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("create dir: " + file.mkdir());
        file.renameTo(new File("./src/com/stupidzhe/jdklearning/io/file-test/k"));
    }
}


/*
        File file = new File("./src/com/stupidzhe/jdklearning/io/file-test");
        System.out.println("FileName: " + file.getName());
        System.out.println("isDirectory: " + file.isDirectory());
        String[] fileNameArray = file.list((dir, name) -> Pattern.matches("^[file].*", name));
        if (null == fileNameArray) {
            return;
        }
        for (String fileName : fileNameArray) {
            System.out.println("fileName: " + fileName + " " + fileName.hashCode());
        }
        Arrays.sort(fileNameArray, ((o1, o2) -> (o1.hashCode() > o2.hashCode()?-1:1)));
        for (String fileName : fileNameArray) {
            System.out.println("fileName: " + fileName + " " + fileName.hashCode());
        }
 */