package com.stupidzhe.jdklearning.io.zip;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by Mr.W on 2017/11/12.
 */
public class GZIPTestClass {
    public static void main(String[] args) {
        BufferedReader reader = null;
        DataOutputStream out = null;
        DataInputStream in = null;
        try {
            reader = new BufferedReader(new FileReader("./src/com/stupidzhe/jdklearning/io/file-test/file2.c"));
            out = new DataOutputStream(new GZIPOutputStream(new FileOutputStream("./src/com/stupidzhe/jdklearning/io/file-test/file2.gzip")));

            String s;
            while (null != (s = reader.readLine())) {
                out.writeUTF(s);
                out.writeUTF("\n");
                out.flush();
            }
            reader.close();
            out.close();

            in = new DataInputStream((new GZIPInputStream(new FileInputStream("./src/com/stupidzhe/jdklearning/io/file-test/file2.gzip"))));
            try {
            while(null != (s = in.readUTF())) {
                System.out.print(s);
            }
            } catch (EOFException ignored) {}

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != reader) {
                    reader.close();
                }
                if (null != out) {
                    out.close();
                }
                if (null != in) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
