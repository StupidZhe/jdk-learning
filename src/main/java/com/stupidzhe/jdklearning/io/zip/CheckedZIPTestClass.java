package com.stupidzhe.jdklearning.io.zip;

import java.io.*;
import java.util.zip.*;

/**
 * Created by Mr.W on 2017/11/12.
 */
public class CheckedZIPTestClass {
    public static void main(String[] args) {
        try {
            CheckedOutputStream checkedOutputStream = new CheckedOutputStream(new FileOutputStream("./src/com/stupidzhe/jdklearning/io/file-test1.zip"),
                    new Adler32());

            ZipOutputStream zipOutputStream = new ZipOutputStream(checkedOutputStream);

            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(zipOutputStream);

            DataInputStream dataInputStream = new DataInputStream(new FileInputStream("./src/com/stupidzhe/jdklearning/io/file-test/file1.c"));

            zipOutputStream.putNextEntry(new ZipEntry("/file-test/file3.c"));

            int tmp;
            while ((tmp = dataInputStream.read()) != -1) {
                bufferedOutputStream.write(tmp);
            }

            bufferedOutputStream.flush();
            bufferedOutputStream.close();

            System.out.println(checkedOutputStream.getChecksum().getValue());


            File f = new File("./src/com/stupidzhe/jdklearning/io/file-test1.zip");
            CheckedInputStream checkedInputStream = new CheckedInputStream(new FileInputStream(f),
                    new Adler32());

            ZipInputStream zipInputStream = new ZipInputStream(checkedInputStream);
            ZipEntry zipEntry;
            ZipFile zipFile = new ZipFile(f);
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                System.out.println("file: " + zipEntry.getName());
                InputStreamReader reader = new InputStreamReader(zipFile.getInputStream(zipEntry));
                int c;
                while ((c = reader.read()) != -1) {
                    System.out.print((char) c);
                }
            }


            System.out.println(checkedInputStream.getChecksum().getValue());


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
