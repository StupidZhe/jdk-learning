package com.stupidzhe.jdklearning.io;

import java.io.*;

/**
 * Created by Mr.W on 2017/11/9.
 */
public class IOClass {
    public static void main(String[] args) throws IOException {

        // 新建文件file1.c
        File file = new File("./src/com/stupidzhe/jdklearning/io/file-test/file1.c");

        // output
        FileOutputStream outputStream = new FileOutputStream(file);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
        bufferedOutputStream.write("hello world!".getBytes());
        bufferedOutputStream.flush();

        // 实例化装饰器类，这里我们使用DataOutputStream
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
        dataOutputStream.writeUTF("hello world!");
        dataOutputStream.flush();
        dataOutputStream.close();

        outputStream.close();

        // 实例化装饰器类，这里我们使用PrintStream
        // 第一个参数：outputStream
        // 第二个参数：是否每次换行时清空缓冲
        PrintStream printStream = new PrintStream(outputStream, false);
        printStream.println("hello world!");
        printStream.println("hello world!");
        printStream.flush();
        printStream.close();

        // input
        FileInputStream fileInputStream = new FileInputStream(file);

        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        byte[] b = bufferedInputStream.readAllBytes();
        System.out.println(new String(b));
        DataInputStream dataInputStream = new DataInputStream(fileInputStream);
        System.out.println(dataInputStream.readUTF());
        byte[] bytes = dataInputStream.readAllBytes();
        String content = new String(bytes);
        System.out.println(content);
        dataInputStream.close();

        fileInputStream.close();
    }
}
