package com.stupidzhe.jdklearning.io;

import java.io.*;

/**
 * Created by Mr.W on 2017/11/10.
 */
public class PrintWriterTestClass {
    public static void main(String[] args) {
        try {
//            PrintWriter printWriter = new PrintWriter("./src/com/stupidzhe/jdklearning/io/file-test/file4.c");
//            printWriter.println("hello my world");
//            printWriter.close();

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            PrintWriter printWriter = new PrintWriter(System.out, true);
            printWriter.println(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            System.setOut(new PrintStream("./src/com/stupidzhe/jdklearning/io/file-test/file4.c"));
            System.out.println("hnifdosihfdsf");
            System.out.println("hnifdosihfdsf");
            System.out.println("hnifdosihfdsf");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
