package com.stupidzhe.jdklearning.io;

import java.io.*;

/**
 * Created by Mr.W on 2017/11/9.
 */
public class ReaderAndWriterClass {

    public static void main(String[] args) throws IOException {

        File file = new File("./src/com/stupidzhe/jdklearning/io/file-test/file2.c");

        FileWriter fileWriter = new FileWriter(file, false);
        String content = "你好,世界!\n我爱你！";
        fileWriter.write(content);
        fileWriter.flush();
        fileWriter.close();
        FileReader fileReader = new FileReader(file);
        int i;
        while ((i = fileReader.read()) != -1)
            System.out.print((char) i);
        fileReader.close();

        System.out.println();

        String content2 = "sb sb sb sb sb\nsb sb sb sb sb";
        StringWriter stringWriter = new StringWriter();
        stringWriter.write(content2);
        System.out.println(stringWriter.toString());
    }
}
