package com.stupidzhe.jdklearning.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Mr.W on 2017/11/10.
 */
public class BufferTestClass {
    public static void main(String[] args) {

        try {
            FileReader fileReader = new FileReader("./src/com/stupidzhe/jdklearning/io/file-test/file4.c");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            bufferedReader.close();
            fileReader.close();
            String content = "";
            while (null != (content = bufferedReader.readLine())) {
                System.out.println(content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
