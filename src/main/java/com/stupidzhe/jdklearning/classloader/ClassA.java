package com.stupidzhe.jdklearning.classloader;

/**
 * @Author: StupidZhe
 * @Date: Created in 2018/3/27
 * @Description:
 */
public class ClassA {
    static {
        System.out.println("ClassA is Load");
    }

    static String name = "ClassA";

    static void print() {
        System.out.println("ClassA is printing");
    }
}

class ClassB extends ClassA {
    static {
        System.out.println("ClassB is Load");
    }
}
