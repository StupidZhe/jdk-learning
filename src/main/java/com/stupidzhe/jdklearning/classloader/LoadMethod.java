package com.stupidzhe.jdklearning.classloader;

/**
 * @Author: StupidZhe
 * @Date: Created in 2018/3/27
 * @Description:
 */
public class LoadMethod {

    static {
        System.out.println("LoadMethod is Load");
    }

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {

//        Class<ClassA> classAClass = ClassA.class;
        // 1.第一次实例化类
//        ClassA classA = new ClassA();

        // 5.反射
        Class<ClassA> classAClass = (Class<ClassA>) Class.forName("com.stupidzhe.jdklearning.classloader.ClassA");
//        System.out.println(ClassLoader.getSystemClassLoader().getName());
        System.out.println(ClassLoader.getSystemResource("stupidzhe"));
        // 3.调用类中的静态变量
//        System.out.println(ClassA.name);

        // 4.调用类中的静态方法
//        ClassA.print();

        // 5.初始一个类的子类
//        ClassB classB = new ClassB();

        // 6.该类被作为启动类
    }
}
