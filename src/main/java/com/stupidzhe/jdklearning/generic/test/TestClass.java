package com.stupidzhe.jdklearning.generic.test;

import com.stupidzhe.jdklearning.generic.IncludeGMethodClass;

/**
 * Created by Mr.W on 2017/11/5.
 */
public class TestClass {

    public TestClass() {
        print();
    }

    public void print() {
        System.out.println("3");
    }

    public static void main(String[] args) {
        IncludeGMethodClass<String> includeGMethodClass = new IncludeGMethodClass<>();
        IncludeGMethodClass<String>.C<Integer> c = includeGMethodClass.new C<Integer>();
        System.out.println(c.a);
    }
}
