package com.stupidzhe.jdklearning.generic;

import java.util.Arrays;

/**
 * Created by Mr.W on 2017/11/3.
 */
public class IncludeGMethodClass<A> {

    public class C<A> {
        public A a;
    }

    // 泛型方法需要申明泛型
    public static <A> void print(A a) {
        System.out.println(a.toString());
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(new IncludeGMethodClass<String>().getClass().getTypeParameters()));
        IncludeGMethodClass<String> res = new IncludeGMethodClass<>();
        IncludeGMethodClass<String>.C<Integer> a = res.new C<Integer>();
    }
}
