package com.stupidzhe.jdklearning.generic;

import java.util.Stack;

/**
 * Created by Mr.W on 2017/11/3.
 */
public class SimpleGenericClass<T> {
    private T a;
    public SimpleGenericClass(T a) {
        this.a = a;
    }

    public T getA() {
        return a;
    }

    public void setA(T a) {
        this.a = a;
    }

    public static void main(String[] args) {
        SimpleGenericClass<String> stringSimpleGenericClass = new SimpleGenericClass<>("hello");
        Stack<String> stack = new Stack<>();
    }
}
