package com.stupidzhe.jdklearning.map;

/**
 * Created by Mr.W on 2017/9/27.
 * 创建一个功能简单的类似hashmap的map对象
 * warming: 非线程安全的
 */
public interface SimpleHashMap<K, V> {

    /**
     * 放入键值对
     * @param key 键
     * @param val 值
     * @return V 返回被覆盖的值，如果没有则返回null
     */
    V put(K key, V val);

    /**
     * 获取键对应的值
     * @param key 键
     * @return 值
     */
    V get(K key);

    /**
     * 删除指定key对应的entry
     * @param key
     * @return
     */
    V remove(K key);
}
