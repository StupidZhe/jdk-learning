package com.stupidzhe.jdklearning.base.integer;

public class IntegerMain implements Comparable<IntegerMain> {

    // jvm默认-128-127会缓存在内存中，
    // 所以Integer.valueOf(0)时，会直接从内存中获取值为0的Integer对象。
    // 通过 VM 参数-XX:AutoBoxCacheMax=<size> 可以配置缓存的最大值。\\
    // Integer extend Number implements Object
    // 其中Number是所有数字类型相关的类的父类，是抽象类
    // Object我就不想说了
    // 其次，Integer还扩展了Comparable接口，用于两个Integer对象的比较
    public static void main(String[] args) {
        Integer i = Integer.valueOf(-129);
        Integer h =Integer.valueOf(-129);
        if (i == h) {
            System.out.println("i == h");
        }

        Integer i1 = Integer.valueOf(-128);
        Integer h1 =Integer.valueOf(-128);
        if (i1 == h1) {
            System.out.println("i1 == h1");
        }
    }

    @Override
    public int compareTo(IntegerMain o) {
        return 0;
    }
}
