package com.stupidzhe.jdklearning.base.String;

import java.math.BigDecimal;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/28
 * @Description:
 */
public class InternTest {


    public static void main(String[] args) {
        String str1 = "a";
        String str2 = "b";
        String str3 = "ab";
        String str4 = str1 + str2;
        String str5 = new String("ab");
        String str6 = str1 + str2;
        String str7 = "a" + "b";

//        System.out.println(str5.equals(str3));
//        System.out.println(str5 == str3);
//        System.out.println(str5.intern() == str3);
//        System.out.println(str5.intern() == str4);
        System.out.println(str5 == str3.intern());

        float a =0.1f;
        System.out.println(a);
        System.out.println(0.1 - 0.1f);
        System.out.println(1.7f - 0.1f);
        System.out.println(0.12345678901f);
        BigDecimal bigDecimal = new BigDecimal("123");
        int aa = Integer.valueOf("123");
        System.out.println(aa);

    }
}
