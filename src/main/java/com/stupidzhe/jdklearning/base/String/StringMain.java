package com.stupidzhe.jdklearning.base.String;

/**
 * Created by Mr.W on 2017/10/23.
 */
public class StringMain {

    public static String upCase(StringBuilder name) {
        name.append("!");
        return name.toString();
    }

    public static void main(String[] args) {
        // StringBuilder name = new StringBuilder(10000);
        StringBuilder name = new StringBuilder("Jack");
        String newBName = StringMain.upCase(name);
        System.out.println(name);
        System.out.println(newBName);
    }
}