package com.stupidzhe.jdklearning.base.array;

import java.util.Arrays;

/**
 * Created by Mr.W on 2017/11/5.
 */
public class TestClass {
    public static void main(String[] args) {
        String[] array = new String[10];
        String[] array2 = new String[20];
        Arrays.fill(array, "hola");
        Arrays.fill(array2, "wow");
        System.arraycopy(array, 0, array2, 10, 10);
        System.out.println(Arrays.toString(array2));
        System.out.println(Arrays.equals(array, array2));
        Arrays.sort(array2);
        System.out.println(Arrays.toString(array2));
        Arrays.sort(array2, String.CASE_INSENSITIVE_ORDER);
        System.out.println(Arrays.toString(array2));

        int res = Arrays.binarySearch(array2, "hola");
        System.out.println(res);
    }
}