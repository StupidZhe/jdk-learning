package com.stupidzhe.jdklearning.base.collection;

import java.util.LinkedHashMap;

/**
 * Created by Mr.W on 2017/11/7.
 */
//        HashMap<String, String> map = new HashMap<>();
//        map.put("i", "John");
//        map.put("u", "Marry");
//        System.out.println(map.get("i"));
//
//        // toString方法别覆盖为打印所有键值对
//        System.out.println(map.toString());

//        TreeMap<MapExample, String> treeMap = new TreeMap<>((o1, o2) -> (o1.c > o2.c?-1:1));
//        MapExample e1 = new MapExample();
//        MapExample e2 = new MapExample();
//        MapExample e3 = new MapExample();
//        MapExample e4 = new MapExample();
//        MapExample e5 = new MapExample();
//        treeMap.put(e1, "1");
//        treeMap.put(e3, "3");
//        treeMap.put(e2, "2");
//        treeMap.put(e4, "4");
//        treeMap.put(e5, "5");
//        System.out.println(treeMap.toString());
public class MapExample {

    private static int index = 0;
    private int c;

    public MapExample() {
        c = index++;
    }

    public static void main(String[] args) {
        /**
         * 参数1：initialCapacity,初始时的容量
         * 参数2：loadFactor,加载因子
         * 参数3：accessOrder, true:LRU算法进行的排序 false:插入排序
         */
        LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>(2, 0.5f, true);
        linkedHashMap.put("1", "a");
        linkedHashMap.put("2", "b");
        linkedHashMap.put("3", "c");
        linkedHashMap.put("4", "d");
        System.out.println(linkedHashMap);
        linkedHashMap.get("1");
        linkedHashMap.get("2");
        linkedHashMap.get("1");
        System.out.println(linkedHashMap);

    }
}
