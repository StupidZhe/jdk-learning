package com.stupidzhe.jdklearning.base.collection;

import java.util.*;

/**
 * Created by Mr.W on 2017/11/6.
 */
public class TestClass {
    public static void main(String[] args) {
        List<TestClass> list = new LinkedList<>(Collections.nCopies(4, new TestClass()));
        System.out.println(Arrays.toString(list.toArray()));
        TestClass testClass = new TestClass();
        Collections.fill(list, testClass);

        list.add(new TestClass());

        System.out.println(Arrays.toString(list.toArray()));

        if (list.contains(testClass)) {
            System.out.println("contains: " + true);
        }

        list.clear();

        list.get(0);
        if (list.isEmpty()) {
            System.out.println("isEmpty" + true);
        }
    }
}
