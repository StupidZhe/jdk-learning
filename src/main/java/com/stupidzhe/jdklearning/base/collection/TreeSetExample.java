package com.stupidzhe.jdklearning.base.collection;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by Mr.W on 2017/11/6.
 */
public class TreeSetExample implements Comparable<TreeSetExample> {

    private static int index = 0;
    private int c;

    public TreeSetExample() {
        c = index++;
    }

    public static void main(String[] args) {
        SortedSet<TreeSetExample> sortedSet = new TreeSet<>();
        TreeSetExample t1 = new TreeSetExample();
        TreeSetExample t2 = new TreeSetExample();
        TreeSetExample t3 = new TreeSetExample();
        TreeSetExample t4 = new TreeSetExample();
        sortedSet.add(t1);
        sortedSet.add(t2);
        sortedSet.add(t3);
        sortedSet.add(t4);
        for (TreeSetExample treeSetExample : sortedSet) {
            System.out.println(treeSetExample.c);
        }
    }

    @Override
    public int compareTo(TreeSetExample o) {
        if (o.equals(this)) {
            return 0;
        }
        if (o.c == this.c) {
            return 0;
        }
        return o.c < this.c?-1:1;
    }
}
