package com.stupidzhe.jdklearning.base.collection;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by Mr.W on 2017/11/6.
 */


//        Queue<String> queue = new LinkedList<>();
//        queue.add("2");
//        queue.add("1");
//        queue.add("1");
//        queue.add("3");
//        System.out.println(queue.peek());
//        System.out.println(queue.poll());
//        System.out.println(queue.poll());
//        System.out.println(queue.poll());
//        System.out.println(queue.poll());
//        System.out.println(queue.poll());

public class QueueExample {

    private static int index = 0;
    private int c;

    public QueueExample() {
        c = index++;
    }

    public String test(FInterface fInterface, String content) {
        return fInterface.print(content);
    }

    public static void main(String[] args) {

//        LinkedList<String> deque = new LinkedList<>();
//        deque.addFirst("1");
//        deque.addFirst("2");
//        System.out.println(deque.removeFirst());
//        System.out.println(deque.removeFirst());
//        deque.addLast("3");
//        deque.addLast("4");
//        System.out.println(deque.removeFirst());
//        System.out.println(deque.removeFirst());
//
//        deque.addLast("5");
//        deque.addLast("6");
//        System.out.println(deque.removeLast());
//        System.out.println(deque.removeLast());
        Queue<QueueExample> queue = new PriorityQueue<>(10, (o1, o2) -> (o1.c >= o2.c)?1:-1);
        queue.add(new QueueExample());
        queue.add(new QueueExample());
        queue.add(new QueueExample());
        queue.add(new QueueExample());
        System.out.println(queue.poll().test((o) -> o + "1", "test"));
        System.out.println(queue.poll().c);
        System.out.println(queue.poll().c);
        System.out.println(queue.poll().c);

    }
}
interface FInterface {
    public String print(String n);
}