package com.stupidzhe.jdklearning.base.collection;

import java.util.HashSet;

/**
 * Created by Mr.W on 2017/11/6.
 */
public class HashSetExample {

    private int code;

    private static int index;

    public HashSetExample() {
        code = index++;
    }

    public static void main(String[] args) {
        HashSet<HashSetExample> hashSet = new HashSet<>();
        HashSetExample example1 = new HashSetExample();
        HashSetExample example2 = new HashSetExample();
        HashSetExample example3 = new HashSetExample();
        hashSet.add(example1);
        hashSet.add(example2);
        hashSet.add(example3);
        System.out.println(example1.hashCode());
        System.out.println(hashSet.contains(example3));


    }

    @Override
    public int hashCode() {
        return code;
    }
}
