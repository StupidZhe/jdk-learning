package com.stupidzhe.jdklearning.enu;

import java.util.EnumMap;

public class ExtendsTestClass {

    public static void main(String[] args) {
        /*EnumSet<Animal> enumSet = EnumSet.noneOf(Animal.class);
        enumSet.add(Animal.MONKEY);
        enumSet.add(Animal.PIGGER);
        */
        EnumMap<Animal, String> enumMap = new EnumMap<>(Animal.class);
        enumMap.put(Animal.MONKEY, Animal.MONKEY.name());
        System.out.println(enumMap.get(Animal.ELEPHANT));
        System.out.println(Animal.PIGGER.getInfo());
    }
}
