package com.stupidzhe.jdklearning.enu;

public enum Animal {

    PIGGER {
        public String getInfo() {
            return "pigger";
        }

        @Override
        public void wow() {
            System.out.println("hehe");
        }
    },
    MONKEY {
        public String getInfo() {
            return "monkey";
        }
    },
    DOG {
        public String getInfo() {
            return "dog";
        }
    },
    CAT {
        public String getInfo() {
            return "cat";
        }
    },
    ELEPHANT{
        public String getInfo() {
            return "elephant";
        }
    },
    KOLA{
        public String getInfo() {
            return "kola";
        }
    };
    public abstract String getInfo();

    public void wow() {
        System.out.println("haha");
    }

    public void print() {
        System.out.println(this.toString());
    }
}