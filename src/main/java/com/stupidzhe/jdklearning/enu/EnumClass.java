package com.stupidzhe.jdklearning.enu;

import static com.stupidzhe.jdklearning.enu.Animal.*;

public class EnumClass {
    public static void main(String[] args) {
        System.out.println(PIGGER);
        Animal animal = Animal.PIGGER;

        switch (animal) {
            case PIGGER:
                animal.print();
                animal = MONKEY;

            case MONKEY:
                animal.print();
                animal = DOG;

            case DOG:
                animal.print();
                animal = CAT;

            case CAT:
                animal.print();
                animal = ELEPHANT;

            case ELEPHANT:
                animal.print();
                animal = KOLA;

            case KOLA:
                animal.print();
        }
    }
}


