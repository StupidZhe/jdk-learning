package com.stupidzhe.jdklearning.enu;


import java.lang.reflect.Method;
import java.lang.reflect.Type;

/**
 * @author Mr.W
 */
public class EnumMethodTest {
    public static void main(String[] args) {
        Class<Animal> animalClass = Animal.class;
        Method[] methods = animalClass.getMethods();

        System.out.println("---------methods-------");
        for (Method method : methods) {
            System.out.println(method.getName());
        }

        System.out.println("------super class------");
        Class superClass = animalClass.getSuperclass();
        System.out.println(superClass.getName());

        System.out.println("------implements-------");
        Type[] ins = superClass.getGenericInterfaces();
        for (Type impl: ins) {
            System.out.println(impl.getTypeName());
        }

    }
}
