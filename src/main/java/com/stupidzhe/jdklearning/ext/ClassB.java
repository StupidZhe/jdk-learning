package com.stupidzhe.jdklearning.ext;

import java.io.FileNotFoundException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mr.W on 2017/10/30.
 */
public class ClassB extends ClassA {
    @Override
    public void print() throws FileNotFoundException {
        Pattern pattern = Pattern.compile("[1-9]+");
        Matcher matcher = pattern.matcher("1231231 123f");
        System.out.println(matcher.matches());
        System.out.println(Pattern.matches("[1-9]{3}", "123"));
        while (matcher.find()) {
            System.out.println(matcher.group() + " ");
        }
    }

    public static void main(String[] args) {
        ClassB classB = new ClassB();
        try {
            classB.print();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
