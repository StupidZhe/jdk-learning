package com.stupidzhe.jdklearning.ext;

import java.io.FileNotFoundException;
import java.util.Formatter;

/**
 * Created by Mr.W on 2017/10/30.
 */
public class ClassC extends ClassB {

    @Override
    public void print() throws FileNotFoundException {
//        File file = new File("/Users/x72/Desktop/testFile.txt");
        Formatter formatter = new Formatter(System.out);
        formatter.format("%-15s %15s %5.2f\n", "my name is", "Jackson", 1.2222f);
        formatter.format("%-15s %15s %5.2f\n", "your name is", "Wal", 1.222f);
        formatter.format("%-15s %15s %5.2f\n", "his name is", "Jack", 0f);
    }

    public static void main(String[] args) {
        ClassC c = new ClassC();
        try {
            c.print();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
