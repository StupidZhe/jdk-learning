package com.stupidzhe.jdklearning.set;

import com.stupidzhe.jdklearning.map.SimpleHashMap;
import com.stupidzhe.jdklearning.map.SimpleHashMapImpl;

/**
 * Created by Mr.W on 2017/9/27.
 */
public class SimpleHashSetImpl<V> implements  SimpleHashSet<V> {

    private SimpleHashMap<V, Object> map;
    private static final Object PRESENT = new Object();

    public SimpleHashSetImpl() {
        map = new SimpleHashMapImpl<V, Object>();
    }

    @Override
    public boolean add(V val) {
        return map.put(val, PRESENT)==null;

    }

    @Override
    public boolean remove(V val) {
        Object o = map.get(val);
        return o != null && map.remove(val) != null;
    }

    @Override
    public boolean contains(V val) {
        Object o = map.get(val);
        return o != null;
    }
}
