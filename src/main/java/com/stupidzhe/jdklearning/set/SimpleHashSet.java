package com.stupidzhe.jdklearning.set;

/**
 * Created by Mr.W on 2017/9/27.
 * hashSet的简单实现
 */
public interface SimpleHashSet<V> {

    /**
     * 放入数据
     * @param val
     * @return 加入数据，如果本身有该对象，则返回false
     */
    boolean add(V val);

    /**
     * 移除数据
     * @param val
     * @return 如果有，则移除该对象，并返回true
     */
    boolean remove(V val);

    /**
     * 判断包含
     * @param val
     * @return 如果有，则返回true
     */
    boolean contains(V val);
}
