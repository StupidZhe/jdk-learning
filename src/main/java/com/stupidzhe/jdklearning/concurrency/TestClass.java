package com.stupidzhe.jdklearning.concurrency;

import com.stupidzhe.jdklearning.concurrency.task.ExitTask;
import com.stupidzhe.jdklearning.concurrency.task.TestTask;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/17
 * @Description:
 */
public class TestClass {
    public static void main(String[] args) {

        Thread exitThread = new Thread(new ExitTask());
        exitThread.start();
        TestTask testTask = new TestTask();
        ExecutorService executorService = Executors.newCachedThreadPool(r -> {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        });
        for (int n = 0; n < 5; n++) {
            executorService.execute(testTask);
//            Thread taskThread = new Thread(testTask, "no." + n);
//            taskThread.start();
        }
        executorService.shutdown();
        System.out.println("多线程启动");
        while (true) {
            Scanner scanner = new Scanner(System.in);
            try {
                boolean res = scanner.nextBoolean();
                ExitTask.exitExec(res);
            } catch (Exception e) {
                System.out.println("error input");
            }

        }

    }
}
