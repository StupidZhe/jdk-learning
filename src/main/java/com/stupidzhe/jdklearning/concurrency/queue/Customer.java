package com.stupidzhe.jdklearning.concurrency.queue;

import com.stupidzhe.jdklearning.concurrency.queue.queue.ResourceQueue;
import com.stupidzhe.jdklearning.concurrency.queue.res.Resource;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/27
 * @Description: 消费线程
 */
public class Customer implements Runnable {

    private ResourceQueue processQueue, customQueue;

    public Customer(ResourceQueue processQueue, ResourceQueue customQueue) {
        this.processQueue = processQueue;
        this.customQueue = customQueue;
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                Resource resource = processQueue.take();
                resource.FINISH();
                System.out.println(resource);
                customQueue.add(resource);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
