package com.stupidzhe.jdklearning.concurrency.queue;

import com.stupidzhe.jdklearning.concurrency.queue.queue.ResourceQueue;
import com.stupidzhe.jdklearning.concurrency.queue.res.Resource;

import java.util.concurrent.TimeUnit;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/27
 * @Description: 运输线程
 */
public class Transporter implements Runnable {


    public ResourceQueue transportQueue;

    private int count;

    public Transporter(ResourceQueue transportQueue) {
        this.transportQueue = transportQueue;
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                Resource resource = new Resource(++count);
                TimeUnit.SECONDS.sleep(1);
                System.out.println("add new " + resource);
                resource.transport();
                System.out.println(resource);
                transportQueue.add(resource);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
