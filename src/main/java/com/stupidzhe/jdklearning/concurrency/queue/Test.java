package com.stupidzhe.jdklearning.concurrency.queue;

import com.stupidzhe.jdklearning.concurrency.queue.queue.ResourceQueue;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/27
 * @Description: 测试类
 */
public class Test {

    public static void main(String[] args) {
        ResourceQueue transportQueue = new ResourceQueue();
        ResourceQueue processQueue = new ResourceQueue();
        ResourceQueue customQueue = new ResourceQueue();
        ExecutorService executor = Executors.newCachedThreadPool();
        executor.execute(new Customer(processQueue, customQueue));
        executor.execute(new Processor(transportQueue, processQueue));
        executor.execute(new Transporter(transportQueue));

        executor.shutdown();
    }
}
