package com.stupidzhe.jdklearning.concurrency.queue.res;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/27
 * @Description: 资源
 */
public class Resource {

    public int getId() {
        return id;
    }

    public enum STATUS {
        NULL, TRANSPORT, PROCESS, FINISH
    }

    public STATUS status = STATUS.NULL;

    private final int id;

    public Resource(int id) {
        this.id = id;
    }

    public void transport() {
        this.status = STATUS.TRANSPORT;
    }

    public void process() {
        this.status = STATUS.PROCESS;
    }

    public void FINISH() {
        this.status = STATUS.FINISH;
    }

    @Override
    public String toString() {
        return "Resource id:" + id + ":" + status;
    }

}
