package com.stupidzhe.jdklearning.concurrency.queue;

import com.stupidzhe.jdklearning.concurrency.queue.queue.ResourceQueue;
import com.stupidzhe.jdklearning.concurrency.queue.res.Resource;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/27
 * @Description: 加工线程
 */
public class Processor implements Runnable {

    private ResourceQueue transportQueue, processQueue;


    public Processor(ResourceQueue transportQueue, ResourceQueue processQueue) {
        this.transportQueue = transportQueue;
        this.processQueue = processQueue;
    }

    @Override
    public void run() {
        while(!Thread.interrupted()) {
            try {
                Resource resource = transportQueue.take();
                resource.process();
                System.out.println(resource);
                processQueue.add(resource);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
