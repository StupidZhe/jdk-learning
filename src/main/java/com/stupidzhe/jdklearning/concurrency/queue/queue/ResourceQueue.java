package com.stupidzhe.jdklearning.concurrency.queue.queue;

import com.stupidzhe.jdklearning.concurrency.queue.res.Resource;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/27
 * @Description: 存放资源的阻塞队列
 */
public class ResourceQueue extends LinkedBlockingQueue<Resource> {
}
