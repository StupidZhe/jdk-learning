package com.stupidzhe.jdklearning.concurrency.sync;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/25
 * @Description: 动态地使用同步锁
 */
public class SyncTest2 implements Runnable {

    private static int a;

    private Lock lock = new ReentrantLock();

    public void print() {
        // 该操作不是原子性的
        System.out.println(++a);
    }

    @Override
    public void run() {
        Thread.currentThread().interrupt();
        if (Thread.currentThread().isInterrupted()) {
            return;
        }
        while (a < 100) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lock.lock();
            print();
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        SyncTest2 syncTest = new SyncTest2();
        ExecutorService executor = Executors.newCachedThreadPool();
        for (int i = 0; i < 5; i++) {
            executor.execute(new Thread(syncTest));
        }
        executor.shutdown();
    }
}
