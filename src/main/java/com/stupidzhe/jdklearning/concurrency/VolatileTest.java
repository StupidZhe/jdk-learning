package com.stupidzhe.jdklearning.concurrency;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/22
 * @Description:
 */
public class VolatileTest implements Runnable {

    public static AtomicInteger a = new AtomicInteger(0);

    @Override
    public void run() {

        while (a.intValue() < 100) {
            try {
                Thread.sleep(500);
                System.out.println(Thread.currentThread().getName() + ":" + a.incrementAndGet());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        VolatileTest volatileTest = new VolatileTest();
        Thread thread = new Thread(volatileTest);
        thread.start();
        try {
            thread.wait();
            Thread.sleep(10000);
            thread.notify();
        } catch (Exception e) {
            e.printStackTrace();
        }
        new Thread(volatileTest).start();
        new Thread(volatileTest).start();
        new Thread(volatileTest).start();
//        new Thread(new VolatileTest2()).start();
    }
}
