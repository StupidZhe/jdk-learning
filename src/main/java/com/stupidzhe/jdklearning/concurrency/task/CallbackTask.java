package com.stupidzhe.jdklearning.concurrency.task;

import java.util.concurrent.*;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/17
 * @Description:
 */
public class CallbackTask implements Callable<String> {

    private int id;

    public CallbackTask(int id) {
        this.id = id;
    }

    @Override
    public String call() throws Exception {
        Thread.sleep(5000);
        return "the result id = " + id;
    }

}

class Test {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CallbackTask callbackTask = new CallbackTask(1);
        ExecutorService executorService = Executors.newCachedThreadPool();
        Future<String> future = executorService.submit(callbackTask);

        // 结束添加任务
        executorService.shutdown();

        System.out.println("waiting 5 seconds");

        while (!future.isDone()) {
            System.out.println(future.get());
        }
    }

}