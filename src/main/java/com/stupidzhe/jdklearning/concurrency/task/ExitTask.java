package com.stupidzhe.jdklearning.concurrency.task;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/17
 * @Description: 关闭进程任务
 */
public class ExitTask implements Runnable {

    private static boolean exit;

    @Override
    public synchronized void run() {
        Thread.currentThread().setPriority(10);
        while (true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (exit) {

                System.exit(1);
            }
        }
    }

    public synchronized static void exitExec(boolean res) {
        Lock lock = new ReentrantLock();
        lock.lock();
        exit = res;
        lock.unlock();
    }
}
