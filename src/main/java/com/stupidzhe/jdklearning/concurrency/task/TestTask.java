package com.stupidzhe.jdklearning.concurrency.task;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/17
 * @Description: 继承Runnable的任务，用于并发运行
 */
public class TestTask implements Runnable {

    public TestTask() {

    }


    private static int count = 1;

    private int a;

    private Map<String, Integer> map = new HashMap<>(10);


    @Override
    public void run() {

        //        System.out.println(Thread.currentThread().getName() + ":" + Thread.currentThread().getPriority());
        map.put(Thread.currentThread().getName(), 1);
        while (a < 100000) {
            System.out.println(++a);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getName() + ":" + Thread.currentThread().getPriority() + ":" + map.get(Thread.currentThread().getName()));
    }

//    private float addA() {
//        return (float) ((a = 1.0000001f + a) / Math.PI);
//    }

}
