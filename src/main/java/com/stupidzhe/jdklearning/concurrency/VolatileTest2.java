package com.stupidzhe.jdklearning.concurrency;

/**
 * @Author: StupidZhe
 * @Date: Created in 2017/11/22
 * @Description:
 */
public class VolatileTest2 extends Thread {

    public static volatile int a = 0;

    @Override
    public void run() {

        while (a < 100) {
            try {
                Thread.sleep(500);
                System.out.println(Thread.currentThread().getName() + ":" + ++a);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        VolatileTest2 volatileTest = new VolatileTest2();
        new Thread(volatileTest).start();
        new Thread(volatileTest).start();
        new Thread(volatileTest).start();
        new Thread(volatileTest).start();
//        new Thread(new VolatileTest2()).start();
    }

}
