package com.stupidzhe.jdklearning;

import com.stupidzhe.jdklearning.map.SimpleHashMap;
import com.stupidzhe.jdklearning.map.SimpleHashMapImpl;
import com.stupidzhe.jdklearning.set.SimpleHashSet;
import com.stupidzhe.jdklearning.set.SimpleHashSetImpl;

import java.util.HashSet;

/**
 * Created by Mr.W on 2017/9/27.
 */
public class JdkMain {
    public static void main(String[] args) {
        SimpleHashMap<String, String> simpleHashMap = new SimpleHashMapImpl<String, String>();
        System.out.println(simpleHashMap.get("1"));
        simpleHashMap.put("1", "1");
        simpleHashMap.put("1", "1");
        simpleHashMap.put("2", "1");
        System.out.println(simpleHashMap.get("1"));
        HashSet<JdkMain> res = new HashSet<JdkMain>();
        res.add(new JdkMain());
        SimpleHashSet<JdkMain> set = new SimpleHashSetImpl<JdkMain>();
        JdkMain jdkMain2 = new JdkMain();
        set.add(jdkMain2);
        System.out.println(set.contains(jdkMain2));
        System.out.println(set.remove(jdkMain2));
        System.out.println(set.remove(jdkMain2));
    }
}
